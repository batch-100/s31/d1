const coursesData = [
  {
    id: "wdc001",
    name: "PHP - Laravel",
    description:
      "So you two dig up, dig up dinosaurs? Remind me to thank John for a lovely weekend.",
    price: 45000,
    onOffer: true,
  },
  {
    id: "wdc002",
    name: "Python - Django",
    description:
      "So you two dig up, dig up dinosaurs? Remind me to thank John for a lovely weekend.",
    price: 30000,
    onOffer: true,
  },
  {
    id: "wdc003",
    name: "Java - Springboot",
    description:
      "So you two dig up, dig up dinosaurs? Remind me to thank John for a lovely weekend.",
    price: 10000,
    onOffer: true,
  },
];

export default coursesData;

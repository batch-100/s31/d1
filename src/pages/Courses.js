import { Fragment } from 'react';

import Course from '../components/Course';
import coursesData from '../data/data'; //arayy

export default function Courses(){
  //ung coursecards, pinapasahan lang natin ng array
  //ES6 syntax for anon functions
    const CourseCards = coursesData.map((course)=>{ //userdefined ung CourseCards; ung courses daling lang sa data folder
        console.log(course);
        return (
          //course component
          //{eto iung params}, course, key is ung props
          //yung Course may laman  lang siya na objects, eto ung key at course
            <Course key = {course.id} course={course} />
        );
    });
    return (
        <Fragment>
            {CourseCards}
        </Fragment>
    )
}

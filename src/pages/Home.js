import React, { Fragment } from "react";
import { Container } from "react-bootstrap";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import Courses from "./Courses";

function Home() {
  return (
    <Fragment>
      <Container>
        <Banner />
        <Highlights />
        <Courses/>
      </Container>
    </Fragment>
  );
}

export default Home;

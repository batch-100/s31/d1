import React from "react";
import { Jumbotron, Button, Row, Col } from "react-bootstrap";
function Banner() {
  return (
    <div>
      <Row>
        <Col>
          <Jumbotron>
            <h1>Zuitt Bootcamp</h1>
            <p>Opportunities for everyone, everywhere</p>
            <Button variant="primary">Enroll Now!</Button>
          </Jumbotron>
        </Col>
      </Row>
    </div>
  );
}

export default Banner;

import React from 'react';
export class Class extends React.Component{
    render() {
        return(
            <p>This is a class component</p>
        )
    }
}


export class ClassComponent extends React.Component{
    render(){
        return(
            <p>This is a second class component.</p>
        )
    }
}
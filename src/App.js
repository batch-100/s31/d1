import React, { Fragment } from 'react';
import NavBar from './components/NavBar'; //NavBar to kasi ung filename sa loob ng components folder
import Home from './pages/Home';



function App() {
    return (
        <Fragment>
            <NavBar/>
            <Home/>
        </Fragment>
    );
}

export default App;